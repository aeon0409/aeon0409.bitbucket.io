'use strict';

app.factory('DataHolder', function() {
	var families = [];
  var currentFamilydetails = {};
  var familyDetails = {};
  var familyTaggedList = [];
  var familyAdultList = [];

  return {
    setFamilies: function(data) {
      families = data;
    },
    getFamilies: function() {
    	return families;
    },
    setFamily: function(data) {
      familyDetails = data;
    },
    getFamily: function() {
      return familyDetails;
    },
    setFamilyTaggedList: function(data) {
      familyTaggedList = data;
    },
    getFamilyTaggedList: function() {
      return familyTaggedList;
    },
    setCurrentFamilyDetails: function(data) {
      currentFamilydetails = data;
    },
    getCurrentFamilyDetails: function() {
      return currentFamilydetails;
    },
    setFamilyAdultList: function(data) {
      familyAdultList = data;
    },
    getFamilyAdultList: function() {
      return familyAdultList;
    }
    
  };
});
