'use strict';
function contactsCtrl($scope, $location, $rootScope,DataHolder, $stateParams, $state) {
	console.log("Contact CTRL");
	$scope.families = DataHolder.getFamilies();
	$scope.getFamilyList = function() {
		var apiLink = $rootScope.apiLink + '/contacts/FamilyListGet?byPassFilter=true&StartWith=*';
		var request = $.ajax({
            url: apiLink,
            type: 'GET',
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('access_token') }
        });

        request.done(function(ret, textStatus, jqXHR) {
            console.log(ret.FamilyList);
            $scope.families = ret.FamilyList;
            DataHolder.setFamilies($scope.families);
            $scope.$apply();
        });

        request.fail(function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            $rootScope.logout();
        });

	};

    $scope.saveCurrentFamily = function (data){
        console.log(data);
        DataHolder.setCurrentFamilyDetails(data);
        $state.go('app.contactsdetail',{familyID: data.FamilyID});
    };
	if(!$scope.families.length){
		$scope.getFamilyList();
	}
	console.log($scope.families);
	
};
angular
    .module('app')
    .controller('contactsCtrl', contactsCtrl);