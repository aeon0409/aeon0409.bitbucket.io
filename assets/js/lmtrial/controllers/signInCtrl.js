'use strict';
function signInCtrl($scope, $location, $http, $rootScope) {
	console.log("Sign In CTRL");
	$scope.login = function(data) {
		console.log(data,$rootScope.apiLink);
		var apiLink = $rootScope.apiLink + '/Login';
		var request = $.ajax({
            url: apiLink,
            type: 'POST',
            data: data
        });

        request.done(function(ret, textStatus, jqXHR) {
            console.log(ret);
            localStorage.setItem('access_token', ret);
            $location.url('/app/contacts');
            $scope.$apply();

        });

        request.fail(function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            alert("Invalid username or password");
        });

	};
};
angular
    .module('app')
    .controller('signInCtrl', signInCtrl);