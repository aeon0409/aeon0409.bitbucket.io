'use strict';

function contactDetailsCtrl($scope, $location, $rootScope, DataHolder, $stateParams, $state, $uibModal) {
    console.log("Contact Details CTRL");
    console.log($stateParams.familyID);
    $scope.familyDetails = DataHolder.getFamily();
    $scope.familyTaggedList = DataHolder.getFamilyTaggedList();
    $scope.currentFamilyDetails = DataHolder.getCurrentFamilyDetails();
    $scope.familyAdultList = DataHolder.getFamilyAdultList();
    $scope.staticAddress = `123 Unit\nStreet Number 3,\nStreet Name,\nSuburb Name,\nLost City,\n32145 Alabana,\nUSA`;



    $scope.getFamilyDetails = function() {
        $scope.familyTaggedList = [];
        $scope.familyDetails = {};
        $scope.familyAdultList = [];
        var apiLink = $rootScope.apiLink + '/contacts/ContactFamilyInfoGet?familyId=' + $stateParams.familyID;
        var request = $.ajax({
            url: apiLink,
            type: 'GET',
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('access_token') }
        });

        request.done(function(ret, textStatus, jqXHR) {
            console.log(ret);
            $scope.familyDetails = ret;
            DataHolder.setFamily($scope.familyDetails);
            $scope.getFamilyTaggedList();
            $scope.$apply();
        });

        request.fail(function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            $rootScope.logout();
        });

    };

    $scope.getFamilyTaggedList = function() {
        var apiLink = $rootScope.apiLink + '/contacts/TaggedList?familyID=' + $stateParams.familyID;
        var request = $.ajax({
            url: apiLink,
            type: 'GET',
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('access_token') }
        });

        request.done(function(ret, textStatus, jqXHR) {
            console.log(ret);
            $scope.familyTaggedList = ret;
            DataHolder.setFamilyTaggedList($scope.familyTaggedList);
            $scope.$apply();

        });

        request.fail(function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            alert("error getting family tagged list");
        });

    };

    $scope.getFamilyAdultList = function() {
        var apiLink = $rootScope.apiLink + '/contacts/ClientInformGet?familyId=' + $stateParams.familyID;
        var request = $.ajax({
            url: apiLink,
            type: 'GET',
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('access_token') }
        });

        request.done(function(ret, textStatus, jqXHR) {
            console.log(ret);
            $scope.familyAdultList = ret;
            $scope.$apply();
            DataHolder.setFamilyAdultList($scope.familyAdultList);
        });

        request.fail(function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            alert("error getting family tagged list");
        });

    };

    $scope.open = function(size) {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'assets/views/lmtrial/add-client.html',
            size: size,
            controller: 'contactDetailsCtrl',
        });

    };
    $scope.homePhone = {
        Type: "Home"
    };
    $scope.workPhone = {
        Type: "Work"
    };
    $scope.mobilePhone = {
        Type: "Mobile"
    };
    $scope.homeEmail = {
        Type: "Home"
    };
    $scope.DOB;
    $scope.htmlTooltip = "";
    $scope.employment = {};
    $scope.occupationClass;

    $scope.addClient = function(data) {
        // data.FullName = data.FirstName + " " + data.MiddleName + " " + data.LastName;
        if(data === undefined)
            return;
        var phoneData = [];
        if ($scope.occupationClass) {
            console.log("email");
            $scope.employment.OccupationClass =  parseInt($scope.occupationClass);
        }       
        if (!$scope.isObjectEmpty($scope.employment)) {
            console.log($scope.employment);
            data.Employment = [$scope.employment];
        }
        if ($scope.homeEmail.value) {
            console.log("email");
            data.Email = [$scope.homeEmail];
        }
        if ($scope.homePhone.value) {
            console.log("hp");
            phoneData.push($scope.homePhone);
        }
        if ($scope.workPhone.value) {
            console.log("wp");
            phoneData.push($scope.workPhone);
        }
        if ($scope.mobilePhone.value) {
            console.log("mp");
             phoneData.push($scope.mobilePhone);
        }
        if ($scope.DOB) {
            data.DOB = $scope.DOB.toISOString();
        }
        console.log(phoneData);
        data.Phone = phoneData;
        data.FamilyId = $stateParams.familyID;
        data = [data];
        data = JSON.stringify(data);

        console.log(data);
        var apiLink = $rootScope.apiLink + '/contacts/FamilyInfoSet';
        var request = $.ajax({
            url: apiLink,
            type: "POST",
            headers: {
             'Authorization': 'Bearer ' + localStorage.getItem('access_token'),
             'Content-Type': 'application/json'        
                },
            data: data
        });

        request.done(function(ret, textStatus, jqXHR) {
            console.log(ret);
            // $scope.modalInstance.hide();
            // $scope.modalInstance.close();
            $scope.getFamilyAdultList();
            $scope.$apply();
            alert('Success');
        });

        request.fail(function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        });

    };


    $scope.isObjectEmpty = function(card) {
        return Object.keys(card).length === 0;
    }
    $scope.hideGrandParent = function(data) {
        console.log(data);
        $(".close").closest(".smc-noti-container").css({ "padding-top": "1px" });
    }
    console.log($scope.currentFamilyDetails.FamilyID != $stateParams.familyID);
    if ($scope.currentFamilyDetails.FamilyID != $stateParams.familyID) {
        $state.go('app.contacts');
    } else {
        $scope.getFamilyDetails();
    }
    console.log($scope.currentFamilyDetails);
    console.log($scope.familyDetails);
    console.log($scope.familyTaggedList);

    var placeholder = "UNIT/FLAT NUMBER \nSTREET ADDRESS";
    $('textarea.ccma-textarea').attr('placeholder', placeholder);
    $scope.setTextArea = function() {
        $('textarea.ccma-textarea').attr('placeholder', placeholder);
        console.log('set textarea place holder');
    };
    $('textarea').focus(function() {
        if ($(this).val() === placeholder) {
            $(this).attr('value', '');
        }
    });

    $('textarea.ccma-textarea').blur(function() {
        if ($(this).val() === '') {
            $(this).attr('value', placeholder);
        }
    });
};
angular
    .module('app')
    .controller('contactDetailsCtrl', contactDetailsCtrl);