'use strict';

/**
 * Config for the router
 */
app.config(['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'JS_REQUIRES',
function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, jsRequires) {

    app.controller = $controllerProvider.register;
    app.directive = $compileProvider.directive;
    app.filter = $filterProvider.register;
    app.factory = $provide.factory;
    app.service = $provide.service;
    app.constant = $provide.constant;
    app.value = $provide.value;

    // LAZY MODULES

    $ocLazyLoadProvider.config({
        debug: false,
        events: true,
        modules: jsRequires.modules
    });

    // APPLICATION ROUTES
    // -----------------------------------
    // For any unmatched url, redirect to /app/dashboard
    $urlRouterProvider.otherwise("/app/contacts");
    //
    // Set up the states
    $stateProvider.state('app', {
        url: "/app",
        templateUrl: "assets/views/app.html",
        resolve: loadSequence('chartjs', 'chart.js', 'chatCtrl'),
        abstract: true
    }).state('app.dashboard', {
        url: "/dashboard",
        templateUrl: "assets/views/lmtrial/dashboard.html",
        title: 'Dashboard',
        ncyBreadcrumb: {
            label: 'Dashboard'
        },
        controller: function ($scope) {
            $scope.setLayout();
            $scope.app.layout.isSidebarFixed = true;
            $scope.app.layout.isNavbarFixed = true;
        }
    }).state('app.contacts', {
        url: "/contacts",
        templateUrl: "assets/views/lmtrial/contacts.html",
        title: 'Contacts',
        resolve: loadSequence('contactsCtrl'),
        ncyBreadcrumb: {
            label: 'Contacts'
        },
        controller: function ($scope) {
            $scope.setLayout();
            $scope.app.layout.isSidebarFixed = true;
            $scope.app.layout.isNavbarFixed = true;
        }

    }).state('app.contactsdetail', {
        url: "/contacts/:familyID",
        templateUrl: "assets/views/lmtrial/contact-details.html",
        title: 'Contact Details',
        resolve: loadSequence('contactDetailsCtrl'),
        ncyBreadcrumb: {
            label: 'Contact Details'
        },
        controller: function ($scope) {
            $scope.setLayout();
            $scope.app.layout.isSidebarFixed = true;
            $scope.app.layout.isNavbarFixed = true;
        }
    }).state('app.loan-tools', {
        url: "/loan-toals",
        templateUrl: "assets/views/lmtrial/loan-tools.html",
        title: 'Loan Tools',
        ncyBreadcrumb: {
            label: 'Loan Tools'
        },
        controller: function ($scope) {
            $scope.setLayout();
            $scope.app.layout.isSidebarFixed = true;
            $scope.app.layout.isNavbarFixed = true;
        }
    }).state('app.insurance-tools', {
        url: "/insurance-tools",
        templateUrl: "assets/views/lmtrial/insurance-tools.html",
        title: 'Insurance Tools',
        ncyBreadcrumb: {
            label: 'Insurance Tools'
        },
        controller: function ($scope) {
            $scope.setLayout();
            $scope.app.layout.isSidebarFixed = true;
            $scope.app.layout.isNavbarFixed = true;
        }
    }).state('app.workflow', {
        url: "/workflow",
        templateUrl: "assets/views/lmtrial/workflow.html",
        title: 'Workflow',
        ncyBreadcrumb: {
            label: 'Workflow'
        },
        controller: function ($scope) {
            $scope.setLayout();
            $scope.app.layout.isSidebarFixed = true;
            $scope.app.layout.isNavbarFixed = true;
        }
    }).state('app.communicate', {
        url: "/communicate",
        templateUrl: "assets/views/lmtrial/communicate.html",
        title: 'Communicate',
        ncyBreadcrumb: {
            label: 'Communicate'
        },
        controller: function ($scope) {
            $scope.setLayout();
            $scope.app.layout.isSidebarFixed = true;
            $scope.app.layout.isNavbarFixed = true;
        }
    }).state('app.report', {
        url: "/report",
        templateUrl: "assets/views/lmtrial/report.html",
        title: 'Report',
        ncyBreadcrumb: {
            label: 'Report'
        },
        controller: function ($scope) {
            $scope.setLayout();
            $scope.app.layout.isSidebarFixed = true;
            $scope.app.layout.isNavbarFixed = true;
        }
    }).state('app.commission', {
        url: "/commission",
        templateUrl: "assets/views/lmtrial/commission.html",
        title: 'Commission',
        ncyBreadcrumb: {
            label: 'Commission'
        },
        controller: function ($scope) {
            $scope.setLayout();
            $scope.app.layout.isSidebarFixed = true;
            $scope.app.layout.isNavbarFixed = true;
        }
    }).state('app.resources', {
        url: "/resources",
        templateUrl: "assets/views/lmtrial/resources.html",
        title: 'Resources',
        ncyBreadcrumb: {
            label: 'Resources'
        },
        controller: function ($scope) {
            $scope.setLayout();
            $scope.app.layout.isSidebarFixed = true;
            $scope.app.layout.isNavbarFixed = true;
        }
    }).state('app.referrals', {
        url: "/referrals",
        templateUrl: "assets/views/lmtrial/referrals.html",
        title: 'Referrals',
        ncyBreadcrumb: {
            label: 'Referrals'
        },
        controller: function ($scope) {
            $scope.setLayout();
            $scope.app.layout.isSidebarFixed = true;
            $scope.app.layout.isNavbarFixed = true;
        }
    }).state('app.feedback', {
        url: "/feedback",
        templateUrl: "assets/views/lmtrial/feedback.html",
        title: 'Feedback',
        ncyBreadcrumb: {
            label: 'Feedback'
        },
        controller: function ($scope) {
            $scope.setLayout();
            $scope.app.layout.isSidebarFixed = true;
            $scope.app.layout.isNavbarFixed = true;
        }
    }).state('app.users', {
        url: "/users",
        templateUrl: "assets/views/lmtrial/users.html",
        title: 'Users',
        ncyBreadcrumb: {
            label: 'Users'
        },
        controller: function ($scope) {
            $scope.setLayout();
            $scope.app.layout.isSidebarFixed = true;
            $scope.app.layout.isNavbarFixed = true;
        }
    }).state('app.customise-theme', {
        url: "/customise-theme",
        templateUrl: "assets/views/lmtrial/customise-theme.html",
        title: 'Customise Theme',
        ncyBreadcrumb: {
            label: 'Customise Theme'
        },
        controller: function ($scope) {
            $scope.setLayout();
            $scope.app.layout.isSidebarFixed = true;
            $scope.app.layout.isNavbarFixed = true;
        }
    })

	.state('login', {
	    url: '/login',
	    template: '<div ui-view class="fade-in-right-big smooth"></div>',
	    abstract: true
	}).state('login.signin', {
	    url: '/signin',
	    templateUrl: "assets/views/lmtrial/login_login.html",
        resolve: loadSequence('signInCtrl'),
        title: 'Sign In'
	}).state('login.forgot', {
	    url: '/forgot',
	    templateUrl: "assets/views/login_forgot.html"
	}).state('login.registration', {
	    url: '/registration',
	    templateUrl: "assets/views/login_registration.html"
	}).state('login.lockscreen', {
	    url: '/lock',
	    templateUrl: "assets/views/login_lock_screen.html"
	})

	// Landing Page route
	.state('landing', {
	    url: '/landing-page',
	    template: '<div ui-view class="fade-in-right-big smooth"></div>',
	    abstract: true,
	    resolve: loadSequence('jquery-appear-plugin', 'ngAppear', 'countTo')
	}).state('landing.welcome', {
	    url: '/welcome',
	    templateUrl: "assets/views/landing_page.html"
	});
    // Generates a resolve object previously configured in constant.JS_REQUIRES (config.constant.js)
    function loadSequence() {
        var _args = arguments;
        return {
            deps: ['$ocLazyLoad', '$q',
			function ($ocLL, $q) {
			    var promise = $q.when(1);
			    for (var i = 0, len = _args.length; i < len; i++) {
			        promise = promiseThen(_args[i]);
			    }
			    return promise;

			    function promiseThen(_arg) {
			        if (typeof _arg == 'function')
			            return promise.then(_arg);
			        else
			            return promise.then(function () {
			                var nowLoad = requiredData(_arg);
			                if (!nowLoad)
			                    return $.error('Route resolve: Bad resource name [' + _arg + ']');
			                return $ocLL.load(nowLoad);
			            });
			    }

			    function requiredData(name) {
			        if (jsRequires.modules)
			            for (var m in jsRequires.modules)
			                if (jsRequires.modules[m].name && jsRequires.modules[m].name === name)
			                    return jsRequires.modules[m];
			        return jsRequires.scripts && jsRequires.scripts[name];
			    }
			}]
        };
    }
}]);